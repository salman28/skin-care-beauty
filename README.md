# Skin Care Beauty

Olansi Healthcare Co., Ltd. is a premier high-tech healthy and environmental friendly factory for purifier and beauty instrument. Over 10 years’ experience, with an integrated research and development program. Our activities includes research, development, injection, assembling, sales and after sales. We are a pure source of quality solution of beauty care products, air purifier, water purifiers, hydrogen water maker and other heath care products. Now Olansi is one of China’s top 5 largest OEM purification products’ factories. We sell to over 20 countries and key markets and annual turnover is over $100 million.

# Why choose us?

1. Patent: obtain all patents for our products.
2. Experience: Provide OEM and ODM service, such as mold making, injection molding.
3. Certificate: CE,CB, RoHs, SASO,CQC, CCC approval & ISO 9001:2008 certificate.
4. Quality Assurance: 100% Aging test for mass production.
5. Warranty service: One year guarantee period, lifelong after-sales service.
6. Provide support: Provide technical information and technical training support regularly.
7. R&D Department: 30 engineers’ R&D team,the R&D director from Midea group
8. Modern Production Chain

Website :  https://www.all-for-skin-beauty.com